﻿using MarketplaceAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketplaceAPI.Extensions
{
	public static class ModelBuilderExtensions
	{
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Any guid
            var adminId = "0xE944FE15c1b52c6f5E4035B7Ca72AA7ba926E289";


            var hasher = new PasswordHasher<User>();

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = adminId,
                FirstName = "Administrator",
                LastName = "",
                Roles = true,
                Tokens = 0,
                ContractAddress = "0x4fb70A4372037930940363066ed28b0d17f5399F",
                IsActive = false,
                UserName = adminId,
                NormalizedUserName = adminId,
                Email = "admimn@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                EmailConfirmed = true,
                PhoneNumber = "0868723814",
                PasswordHash = hasher.HashPassword(null, "Abc@12345"),
                SecurityStamp = string.Empty,
                UrlAvatar = "/upload/avatar/admin1.jpg",
                Address = "Đà Nẵng"
            });


        }
    }
}
