﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketplaceAPI.ViewModels
{
	public class UserPostTokenViewModel
	{
		[Required]
		public double TokenNumber { get; set; }
	}
}
