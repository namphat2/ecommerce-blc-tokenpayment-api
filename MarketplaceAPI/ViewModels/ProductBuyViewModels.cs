﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketplaceAPI.ViewModels
{
	public class ProductBuyViewModel
	{
		[Required]
		public long ProductCount { get; set; }
	}
}
