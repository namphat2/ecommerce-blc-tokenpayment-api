﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketplaceAPI.ViewModels
{
	public class ProductCreateViewModel
	{
        [Required]
        public string IdProduct { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal? Price { get; set; }
        [Required]
        public string ContractAddress { get; set; }
    }
}
