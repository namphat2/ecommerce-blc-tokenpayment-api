﻿using System;
using MarketplaceAPI.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace MarketplaceAPI.Models
{
    public partial class MarketplaceContext : IdentityDbContext<User,IdentityRole<string>, string>
    {
        public MarketplaceContext()
        {
        }

        public MarketplaceContext(DbContextOptions<MarketplaceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<User> User { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ContractAddress)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Owner)
                    .HasMaxLength(450)
                    .IsUnicode(true);

                entity.Property(e => e.Price).HasColumnType("bigint");

                entity.HasOne(d => d.OwnerNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.Owner)
                    .HasConstraintName("FK__Product__Owner__267ABA7A");
                entity.Property(e => e.ProductCount).ValueGeneratedNever();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(x => x.Id);

            });
            modelBuilder.Entity<IdentityUserClaim<string>>().ToTable("app_user_claims")
                .Property(s => s.ClaimType).HasColumnName("claim_type");
            modelBuilder.Entity<IdentityUserClaim<string>>().Property(s => s.ClaimValue).HasColumnName("claim_value");
            modelBuilder.Entity<IdentityUserClaim<string>>().Property(s => s.Id).HasColumnName("id");
            modelBuilder.Entity<IdentityUserClaim<string>>().Property(s => s.UserId).HasColumnName("user_id");

            modelBuilder.Entity<IdentityUserRole<string>>().ToTable("app_user_roles").HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<IdentityUserRole<string>>().Property(s => s.UserId).HasColumnName("user_id");
            modelBuilder.Entity<IdentityUserRole<string>>().Property(s => s.RoleId).HasColumnName("role_id");

            modelBuilder.Entity<IdentityUserLogin<string>>().ToTable("app_user_logins").HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityUserLogin<string>>().Property(s => s.UserId).HasColumnName("user_id");
            modelBuilder.Entity<IdentityUserLogin<string>>().Property(s => s.LoginProvider).HasColumnName("login_provider");
            modelBuilder.Entity<IdentityUserLogin<string>>().Property(s => s.ProviderDisplayName).HasColumnName("provider_display_name");
            modelBuilder.Entity<IdentityUserLogin<string>>().Property(s => s.ProviderKey).HasColumnName("provider_key");

            modelBuilder.Entity<IdentityRoleClaim<string>>().ToTable("app_role_claims");
            modelBuilder.Entity<IdentityRoleClaim<string>>().Property(s => s.Id).HasColumnName("id");
            modelBuilder.Entity<IdentityRoleClaim<string>>().Property(s => s.RoleId).HasColumnName("role_id");
            modelBuilder.Entity<IdentityRoleClaim<string>>().Property(s => s.ClaimValue).HasColumnName("claim_value");
            modelBuilder.Entity<IdentityRoleClaim<string>>().Property(s => s.ClaimType).HasColumnName("claim_type");

            modelBuilder.Entity<IdentityUserToken<string>>().ToTable("app_user_tokens").HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityUserToken<string>>().Property(s => s.UserId).HasColumnName("user_id");
            modelBuilder.Entity<IdentityUserToken<string>>().Property(s => s.LoginProvider).HasColumnName("login_provider");
            modelBuilder.Entity<IdentityUserToken<string>>().Property(s => s.Name).HasColumnName("name");
            modelBuilder.Entity<IdentityUserToken<string>>().Property(s => s.Value).HasColumnName("value");
            modelBuilder.Entity<IdentityUserRole<string>>().ToTable("app_user_role")
                .HasKey(x => x.UserId);
            OnModelCreatingPartial(modelBuilder);
            modelBuilder.Seed();
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
