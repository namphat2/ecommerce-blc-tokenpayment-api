﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

#nullable disable

namespace MarketplaceAPI.Models
{
    public partial class User : IdentityUser<string>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string UrlAvatar { get; set; }
        public bool? Roles { get; set; }
        public double? Tokens { get; set; }
        public string ContractAddress { get; set; }
        public bool? IsActive { get; set; }
        public virtual IEnumerable<Product> Products { get; set; }
    }
}
