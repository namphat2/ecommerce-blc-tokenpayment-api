﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MarketplaceAPI.Models
{
    public partial class Product
    {
        public Guid Id { get; set; }
        public string IdProduct { get; set; }
        public int? ProductCount { get; set; }
        public string Name { get; set; }
        public string PictureUrl { get; set; }
        public double? Price { get; set; }
        public string Owner { get; set; }
        public bool? IsBuy { get; set; }
        public string ContractAddress { get; set; }
        public User OwnerNavigation { get; set; }
        public string UserBuyer { get; set; }
    }
}
