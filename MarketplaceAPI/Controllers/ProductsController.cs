﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MarketplaceAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MarketplaceAPI.Configurations;
using MarketplaceAPI.ViewModels;
using Microsoft.Data.SqlClient;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace MarketplaceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController : ControllerBase
    {
        private readonly IHostingEnvironment _environment;
        private readonly MarketplaceContext _context;

        public ProductsController(MarketplaceContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Products.Where(x=>x.IsBuy==false).OrderBy(x=>x.ProductCount).ToListAsync();
        }

        [HttpGet("MyProducts")]
        public async Task<ActionResult<IEnumerable<Product>>> GetMyProducts()
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            return await _context.Products.Where(x=>x.Owner == userId).OrderBy(x => x.ProductCount).ToListAsync();
        }

        [HttpGet("PurchasedProduct")]
        public async Task<ActionResult<IEnumerable<Product>>> GetPurchasedProduct()
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            return await _context.Products.Where(x => x.UserBuyer == userId).ToListAsync();
        }
        [HttpGet("SoldProduct")]
        public async Task<ActionResult<IEnumerable<Product>>> GetSoldProduct()
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            return await _context.Products.Where(x => x.Owner == userId && x.IsBuy==true).OrderBy(x => x.ProductCount).ToListAsync();
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(Guid id)
        {
            var product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("BuyProduct/{id}")]
        public async Task<IActionResult> PutProduct(int id)
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            var checkProduct = await _context.Products.Where(x => x.ProductCount == id && x.Owner != userId).ToListAsync();
			if (checkProduct.Count!=1)
			{
                return BadRequest();
			} else
			{
                var myUser = _context.User.Find(userId);
                var myProduct = checkProduct.FirstOrDefault();
                var user2 = _context.User.Find(myProduct.Owner);
                myUser.Tokens = myUser.Tokens - myProduct.Price;
                user2.Tokens = user2.Tokens + myProduct.Price;
                _context.Entry(myUser).State = EntityState.Modified;
                _context.Entry(user2).State = EntityState.Modified;
                var sql = "UPDATE Product SET IsBuy=1,UserBuyer=@UserBuyer WHERE ProductCount=@ProductCount";
                var UserBuyer = new SqlParameter("UserBuyer", userId);
                var ProductCount = new SqlParameter("ProductCount", id);
                _context.Database.ExecuteSqlRaw(sql, new object[] { UserBuyer, ProductCount });
                await _context.SaveChangesAsync();

                return Ok();
            }

        }

        [HttpPut("ImageProduct/{id}")]
        public async Task<IActionResult> PutImageProduct(Guid id,IFormFile file)
        {
			string tokenString = Request.Headers["Authorization"].ToString();
			var infoFromToken = Authorization.GetInfoFromToken(tokenString);
			var userId = infoFromToken.Result.UserId;
			if (file.Length > 0)
            {
                
                    if (!Directory.Exists(_environment.WebRootPath + "/uploads/"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "/uploads/");
                    }
                    using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + "/uploads/" + file.FileName))
                    {
                        file.CopyTo(filestream);
                        filestream.Flush();
                        var sql = "UPDATE Product SET PictureUrl=@Image WHERE Id=@Id AND Owner=@Owner";
						var Image = new SqlParameter("Image", "/uploads/" + file.FileName);
						var Id = new SqlParameter("Id", id);
                        var Owner = new SqlParameter("Owner", userId);
                        _context.Database.ExecuteSqlRaw(sql, new object[] { Image,Id,Owner });
						await _context.SaveChangesAsync();
						return Ok();
                    }
                
            }
            else
            {
                return BadRequest();
            }
            
            

        }

        // POST: api/Products
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(ProductCreateViewModel product)
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            var sql = "Insert into Product (Id,IdProduct,Name,Owner,IsBuy,Price,ContractAddress) values  (NEWID(),@IdProduct,@Name,@Owner,0,@Price,@ContractAddress)";
            var id = new SqlParameter("IdProduct", product.IdProduct);
            var name = new SqlParameter("Name", product.Name);
            var owner = new SqlParameter("Owner", userId);
            var price = new SqlParameter("Price", product.Price);
            var contract = new SqlParameter("ContractAddress", product.ContractAddress);
            _context.Database.ExecuteSqlRaw(sql,new object[] { id,name,owner,price,contract});
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("Contract/{idcontract}")]
        [AllowAnonymous]
        public async Task<IActionResult> DeleteAllContractProduct(string idcontract)
        {
            var product = await _context.Products.Where(x=>x.ContractAddress != idcontract).ToListAsync();
            if (product.Count == 0)
            {
                return NotFound();
            } else
			{
                foreach (var item in product)
                {
                    _context.Products.Remove(item);
                }
                await _context.Database.ExecuteSqlRawAsync("DBCC CHECKIDENT ('Product', RESEED, 0)");
            }
            
            
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(Guid id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
