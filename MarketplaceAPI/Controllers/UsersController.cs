﻿using MarketplaceAPI.Configurations;
using MarketplaceAPI.Models;
using MarketplaceAPI.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MarketplaceAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
    
    public class UsersController : ControllerBase
	{
        private readonly UserManager<User> _userManager;
        private readonly JwtConfig _jwtConfig;
        private readonly MarketplaceContext _context;

        public UsersController(
            UserManager<User> userManager,
            IOptionsMonitor<JwtConfig> optionsMonitor, MarketplaceContext context)
        {
            _userManager = userManager;
            _jwtConfig = optionsMonitor.CurrentValue;
            _context = context;
        }

        [HttpPost]
        [Route("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] UserViewModels user)
        {
            if (ModelState.IsValid)
            {
                var existingPhoneNumber = await _userManager.FindByIdAsync(user.Id);

                if (existingPhoneNumber != null)
                {
                    
                        return BadRequest(new AuthResult()
                        {
                            Errors = new List<string>() {
                                "PhoneNumber already in use"
                            },
                            Success = false
                        });
                    

                }
                var newUser = new User() { Id = user.Id, UserName = user.Id,FirstName=user.FirstName,LastName=user.LastName,ContractAddress=user.ContractAddress,Roles=false,Tokens=0,IsActive=false };

                var isCreated = await _userManager.CreateAsync(newUser, user.Password);
                if (isCreated.Succeeded)
                {
                    var jwtToken = GenerateJwtToken(newUser);

                    return Ok(new AuthResult()
                    {
                        Success = true,
                        Token = jwtToken
                    });
                }
                else
                {
                    return BadRequest(new AuthResult()
                    {
                        Errors = isCreated.Errors.Select(x => x.Description).ToList(),
                        Success = false
                    });
                }
            }

            return BadRequest(new AuthResult()
            {
                Errors = new List<string>() {
                        "Invalid payload"
                    },
                Success = false
            });
        }
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserLoginViewModels user)
        {
            if (ModelState.IsValid)
            {
                var existingUser = await _userManager.FindByIdAsync(user.Id);

                if (existingUser == null)
                {
                    return BadRequest(new AuthResult()
                    {
                        Errors = new List<string>() {
                                "Invalid login request"
                            },
                        Success = false
                    });
                }

                var isCorrect = await _userManager.CheckPasswordAsync(existingUser, user.Password);

                if (!isCorrect)
                {
                    return BadRequest(new AuthResult()
                    {
                        Errors = new List<string>() {
                                "Invalid login request"
                            },
                        Success = false
                    });
                }

                var jwtToken = GenerateJwtToken(existingUser);

                return Ok(new AuthResult()
                {
                    Success = true,
                    Token = jwtToken
                });
            }

            return BadRequest(new AuthResult()
            {
                Errors = new List<string>() {
                        "Invalid payload"
                    },
                Success = false
            });
        }
        [HttpGet]
        [Route("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<User>> Get()
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;

            var quizType = await _context.User.FindAsync(userId);

            if (quizType == null)
            {
                return NotFound();
            }

            return Ok(quizType);
        }
        [HttpDelete]
        [Route("DeleteUsers/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var quizType = await _context.User.FindAsync("0xE944FE15c1b52c6f5E4035B7Ca72AA7ba926E289");
			if (quizType.ContractAddress == id)
			{
                return NotFound();
            } else
			{
                quizType.Tokens = 0;
                quizType.ContractAddress = id;
                quizType.IsActive = false;
                _context.Entry(quizType).State = EntityState.Modified;
                var allUser = await _context.User.Where(x => x.Id != "0xE944FE15c1b52c6f5E4035B7Ca72AA7ba926E289").ToListAsync();
                if (allUser.Count == 0)
                {
                    await _context.SaveChangesAsync();
                    return NotFound();
                }
                else
                {
                    foreach (var item in allUser)
                    {
                        _context.User.Remove(item);
                    }
                    
                }
                
                await _context.SaveChangesAsync();

                return NoContent();
            }
        }

        [HttpPost]
        [Route("PostTokens")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PostRoles(UserPostTokenViewModel model)
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;

            var quizType = await _context.User.FindAsync(userId);

			if (quizType.IsActive == false)
			{
                return BadRequest();
			} else
			{
                quizType.Tokens = quizType.Tokens + model.TokenNumber;
                _context.Entry(quizType).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Ok(quizType);
            }
        }

        [HttpPut]
        [Route("ConfigActive/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ConfigActive(string id)
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            // Get UserId, ChildName, PhoneNumber from token
            var infoFromToken = Authorization.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;

            var quizType = await _context.User.FindAsync(userId);

            if (quizType.Roles == false)
            {
                return BadRequest();
            }
            else
            {
                var users = await _context.User.FindAsync(id);
                if (users.IsActive == true) {
                    return BadRequest();
                } else
				{
                    users.IsActive = true;
                    _context.Entry(users).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                return Ok(users);
            }
        }
        [HttpGet]
        [Route("GetAllUsers")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUser()
        {

            return await _context.User.Where(x => x.IsActive == false).ToListAsync();
        }
        [HttpGet]
        [Route("GetId/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<User>> GetUser(string id)
        {

            var quizType = await _context.User.FindAsync(id);

            if (quizType == null)
            {
                return NotFound();
            }

            return Ok(quizType);
        }
        private string GenerateJwtToken(User user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Id", user.Id),
                    new Claim("Roles",user.Roles.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }
    }
}
