﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace MarketplaceAPI.Configurations
{
    public class Authorization
    {
        
        public static async Task<(string UserId, long Expires)> GetInfoFromToken(string tokenString)
        {
            (string Id, long Expires) result = (string.Empty, 0);

            if (!string.IsNullOrEmpty(tokenString))
            {
                var jwtEncodedString = tokenString.Replace("Bearer ", "");
                var token = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);
                result.Id = token.Claims.First(c => c.Type == "Id").Value;
                result.Expires = long.Parse(token.Claims.First(c => c.Type == "exp").Value);
            }

            return await Task.FromResult(result);
        }
    }
}