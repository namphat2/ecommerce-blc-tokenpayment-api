namespace MarketplaceAPI.Configurations
{
    public class JwtConfig
    {
        public string Secret { get; set; }
    }
}